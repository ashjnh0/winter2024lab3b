import java.util.Scanner;

public class VirtualPetApp {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        Dog[] kennel = new Dog[4];

        for (int i = 0; i < kennel.length; i++) {

            System.out.println("Enter the name of the dog");
            String name = reader.nextLine();

            System.out.println("Enter the age of the dog");
            int age = reader.nextInt();

            reader.nextLine();

            System.out.println("Enter the breed of the dog");
            String breed = reader.nextLine();

            kennel[i] = new Dog(name, age, breed);

            System.out.println("The dog's name is " + kennel[i].name);
            System.out.println("The dog's age is " + kennel[i].age);
            System.out.println("Your dog breed is " + kennel[i].breed);
        }
        System.out.println("Your dog's name is " + kennel[3].name);
        System.out.println(kennel[3].name + "'s human age is " + kennel[3].age);
        System.out.println(kennel[3].name + "'s breed is " + kennel[3].breed);
		
		String revname = kennel[0].reverseDogName();
		int dogAge = kennel[0].dogAgeCalculator();
		
		System.out.println("The first dog's name in dog language is : " + revname);
		System.out.println("The first dog's human age is: " + dogAge);
    }
}
