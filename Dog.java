public class Dog {
	public int age;
	public String breed;
	public String name;
	
	public Dog(String name, int age, String breed ){
		this.age = age;
		this.breed = breed;
		this.name = name;
	}
	
	public String reverseDogName() {
		String reverse = "";
        for (int i = name.length() - 1; i >= 0; i--) {
            reverse += name.charAt(i);
        }
        return reverse;
    }
	
	public int dogAgeCalculator() {
		int humanAge = (age * 16) + 31;
		return humanAge;
	}
}